package scheduler;

import java.io.*;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ClosedChannelException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yuri on 05.03.2016.
 */
public abstract class Kernel implements Runnable, Serializable{
    private static final long serialVersionUID = 1L;
    protected transient JobScheduler js;
    protected Kernel parent;
    protected Kernel target;
    protected boolean reactCondition;
    protected boolean rollbackCondition;
    protected Integer state;
    protected LocalDateTime ts;
    protected Integer _id;
    protected Integer rev;
    private ObjectOutputStream oos;
    protected LocalDateTime restoreTime;

    public Kernel () {}
    public void setRestoreTime(LocalDateTime t){
        restoreTime = t;
    }

    public Kernel (JobScheduler _js, Kernel pk){
        js = _js;
        parent = pk;
        target = this;
        reactCondition = false;
        rollbackCondition = false;
        _id = this.hashCode();
        rev = 0;
        state = 0;
        setTimestamp();
    }

    public void setJs (JobScheduler _js) {
        js = _js;
    }

    public void setTimestamp () {
        ts = LocalDateTime.now();
    }
    public LocalDateTime getTimestamp () {
        return ts;
    }
    public abstract Status act();
    public abstract void react(Kernel ker);
    public abstract void rollback();

    public void run () {
        if (target == null) {
            if (reactCondition){
                System.out.println("here's the end of task: ");
                state = 2;
                logKernel(this);
            }
        } else if (rollbackCondition) {
//            System.out.format("rollback of kernel %s, started\n", this.get_id().toString());
            if (state == 0){
//                System.out.format("kernel %s without act completion\n", this.toString());
                rollback();
                boolean hasReact = act().hasChild;
                target = parent;
                state = 1;
                if (!hasReact){
                    reactCondition = true;
                    js.sendReact(this);
                } else {
                    logKernel(this);
                }
            } else {
//                System.out.format("kernel %s without react completion\n", this.toString());
                parent.react(this);
                parent.rev++;
                logKernel(parent);
                state = 2;
                logKernel(this);
            }

        } else {
            if (target.equals(parent) && reactCondition) {
                parent.react(this);
                parent.rev++;
                logKernel(parent);
                state = 2;
                logKernel(this);
            } else if(!target.equals(parent)){
                boolean hasReact = act().hasChild;
                target = parent;
                state = 1;
                if (!hasReact){
                    reactCondition = true;
                    js.sendReact(this);
                } else {
                    logKernel(this);
                }
            }
        }
    }

    public void logKernel (Kernel k) {
        try {
            k.setTimestamp();
            oos.reset();
            oos.writeObject(k);
            oos.flush();
        } catch (IOException e) {
            if (e.getClass() == ClosedByInterruptException.class || e.getClass() == ClosedChannelException.class){
                System.out.println("stream was closed");
            } else {
                e.printStackTrace();
            }
        }
    }

    public Integer get_id() {
        return _id;
    }
    public void set_id(Integer _id) {
        this._id = _id;
    }

    public Integer getRev() {
        return rev;
    }
    public void setRev(Integer rev) {
        this.rev = rev;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        try {
            oos.writeInt(state);
            oos.writeBoolean(reactCondition);
            oos.writeObject(ts);
            if (parent != null) {
                oos.writeObject(parent.get_id());
            } else {
                oos.writeObject(null);
            }
            if (target != null) {
                oos.writeObject(target.get_id());
            } else {
                oos.writeObject(null);
            }
            oos.writeInt(_id);
            oos.writeInt(rev);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    private void readObject(ObjectInputStream ios) throws IOException, ClassNotFoundException {
        //Add all needed to be logged fields here
        try{
            state = ios.readInt();
            reactCondition = ios.readBoolean();
            ts = (LocalDateTime) ios.readObject();
            parent = new EmptyKernel((Integer) ios.readObject());
            target = new EmptyKernel((Integer) ios.readObject());
            _id = ios.readInt();
            rev = ios.readInt();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public ObjectOutputStream getOos() {
        return oos;
    }
    public void setOos(ObjectOutputStream oos) {
        this.oos = oos;
    }

    @Override
    public String toString() {
        return this.getClass()+"{" +
                "parent=" + (parent == null ? null : (parent._id + " " + parent.getClass())) +
                ", target=" + (target == null ? null : (target._id + " " + target.getClass())) +
                ", reactCondition=" + reactCondition +
                ", rollbackCondition=" + rollbackCondition +
                ", state=" + state +
                ", _id=" + _id +
                ", rev=" + rev +
                '}';
    }
}
