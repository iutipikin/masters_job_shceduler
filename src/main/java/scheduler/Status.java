package scheduler;

/**
 * Created by iutipikin on 4/30/16.
 */
public class Status {
    public boolean hasChild = false;
    public Status (boolean _hasChild) {
        hasChild = _hasChild;
    }
}
