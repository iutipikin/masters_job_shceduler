package scheduler;

import java.io.*;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

/**
 * Created by yurii on 08.04.16.
 */
public class JobScheduler {
    private int _poollsize;
    private BlockingQueue<Kernel> jQueue;
    private PoolWorker [] pool;
    private PoolReactWorker [] reactPool;
    ObjectOutputStream oos;
    private String _dir;
    private Path jsLogPath;
    Integer callCount = 0;


//    local or remote resource provider sets' here
    public JobScheduler (int  ps, String path){
        _dir = path;
        _poollsize = ps;
        jQueue = new LinkedBlockingQueue<>();
        pool = new PoolWorker[_poollsize];
        reactPool = new PoolReactWorker[_poollsize];
        jsLogPath = Paths.get(_dir+"/"+Thread.currentThread().getId() + "-js-log.log");

//        Check if logs already exists and have some undone kernels
        if (Files.isDirectory(Paths.get(_dir))){
            //Restoration process here
            System.out.println("Restoration started");
            Map<Integer, Kernel> restoreMap = new ConcurrentHashMap<>();

            try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(_dir), "*.log")) {
                for (Path currentPath: dirStream) {
                    ObjectInputStream ios;
                    try (InputStream in = new BufferedInputStream(Files.newInputStream(currentPath))) {
                        int numKernels = 0;
                        try {
//                            Object input stream init
                            ios = new ObjectInputStream(in);
                            while (true) {
                                try {
//                                    Read object from log and update previously saved kernel to latest revision
                                    Kernel smth = (Kernel) ios.readObject();
                                    numKernels++;
                                    if (!restoreMap.containsKey(smth.get_id())) {
                                        restoreMap.put(smth.get_id(), smth);
                                    }
                                    if (restoreMap.get(smth.get_id()).rev < smth.rev || smth.state > restoreMap.get(smth.get_id()).state) {
                                        restoreMap.replace(smth.get_id(), restoreMap.get(smth.get_id()), smth);
                                    }
                                } catch (EOFException ex) {
                                    ios.close();
                                    throw new EOFException();
                                } catch (ClassNotFoundException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (EOFException ex) {
                            in.close();
                            System.out.format("Read %d kernels from %s \n", numKernels,currentPath.toString());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                    e.printStackTrace();
            }
            LocalDateTime t2 = LocalDateTime.now();
            restoreMap.forEach((k, v) -> {
                if (v.state == 2) {
                    restoreMap.remove(k, v);
                } else {
                    if (v.parent.get_id() != null) {
                        v.parent = restoreMap.get(v.parent.get_id());
                    } else {
                        v.parent = null;
                        v.setRestoreTime(t2);
                    }
                    if (v.target.get_id() != null) {
                        v.target = restoreMap.get(v.target.get_id());
                    } else {
                        v.target = null;
                    }
                    v.setJs(this);
                }
            });
            oosInit();
            restoreMap.forEach((id, kernel) -> {
                if (kernel.state == 1 && !kernel.reactCondition) {
                    send(kernel);
                } else {
                    kernel.rollbackCondition = true;
                    send(kernel);
                }
            });
            System.out.println("Restoration process complete");
        } else {
            try {
                Files.createDirectory(Paths.get(_dir));
                oosInit();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (int i=0; i<_poollsize; i++) {
            pool[i] = new PoolWorker();
            reactPool[i] = new PoolReactWorker();
            pool[i].start();
            reactPool[i].start();
        }
    }
    private void oosInit() {
        if (Files.exists(jsLogPath)){
            ObjectInputStream ios;
            boolean isCreated = false;

            try (InputStream in = new BufferedInputStream(Files.newInputStream(jsLogPath))) {
                ios = new ObjectInputStream(in);
                Kernel k = (Kernel) ios.readObject();
            } catch (IOException e) {
                if (e.getClass() == StreamCorruptedException.class || e.getClass() == EOFException.class){
                    try {
                        OutputStream out = new BufferedOutputStream(
                                Files.newOutputStream(jsLogPath));
                        oos = new ObjectOutputStream(out);
                        isCreated = true;
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } finally {
                        System.out.format("stream was not initiolize properly \n");
                    }
                } else {
                    System.out.format("Another type of IO exception occured %s", e.getClass());
                    return;
                }
            } catch (ClassNotFoundException e) {
                System.out.format("CNF exception while reading %s", jsLogPath);
                return;
            }
            if (!isCreated){
                try {
                    OutputStream out = new BufferedOutputStream(
                            Files.newOutputStream(jsLogPath, CREATE, APPEND));
                    oos = new AppendingObjectOutputStream(out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                Files.createFile(jsLogPath);
                OutputStream out = new BufferedOutputStream(
                        Files.newOutputStream(jsLogPath));
                oos = new ObjectOutputStream(out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void killAll (long _id) {
        System.out.println("thread id: " + _id + " started an interruption proccess");
        for (PoolWorker pw : pool){
            if (pw.getId() != _id) {
                pw.interrupt();
            }
        }
        for (PoolReactWorker pw : reactPool){
            if (pw.getId() != _id) {
                pw.interrupt();
            }
        }
    }

    public synchronized void send(Kernel k) {
        try {
            jQueue.put(k);
            k.setTimestamp();
            oos.reset();
            oos.writeObject(k);
            oos.flush();
            callCount++;
//            System.out.println("added element action " + k.get_id() +"-" +k.getRev());
//            System.out.println("callCount " + callCount);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public synchronized void sendReact(Kernel k){
        int _id = 0;
        if (k.parent != null){
            _id = k.parent.get_id() % _poollsize;
        }
        try{
            reactPool[_id].send(k);
            k.setTimestamp();
            oos.reset();
            oos.writeObject(k);
            oos.flush();
            callCount++;
//            System.out.println("added element reaction " + k.get_id() +"-" +k.getRev());
//            System.out.println("callCount " + callCount);
        } catch (InterruptedException e){
            Thread.currentThread().interrupt();
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private class PoolWorker extends Thread {
        Path logPath;
        ObjectOutputStream oos;

        public void run() {
            logPath = Paths.get(_dir+"/"+Thread.currentThread().getId() + "-log.log");
            if (Files.exists(logPath)){
                ObjectInputStream ios;
                boolean isCreated = false;

                try (InputStream in = new BufferedInputStream(Files.newInputStream(logPath))) {
                    ios = new ObjectInputStream(in);
                    Kernel k = (Kernel) ios.readObject();
                } catch (IOException e) {
                    if (e.getClass() == StreamCorruptedException.class || e.getClass() == EOFException.class){
                        try {
                            OutputStream out = new BufferedOutputStream(
                                    Files.newOutputStream(logPath));
                            oos = new ObjectOutputStream(out);
                            isCreated = true;
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        } finally {
                            System.out.format("stream was not initiolize properly \n");
                        }
                    } else {
                        System.out.format("Another type of IO exception occured %s", e.getClass());
                        return;
                    }
                } catch (ClassNotFoundException e) {
                    System.out.format("CNF exception while reading %s", logPath);
                    return;
                }
                if (!isCreated){
                    try {
                        OutputStream out = new BufferedOutputStream(
                                Files.newOutputStream(logPath, CREATE, APPEND));
                        oos = new AppendingObjectOutputStream(out);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    Files.createFile(logPath);
                    OutputStream out = new BufferedOutputStream(
                            Files.newOutputStream(logPath));
                    oos = new ObjectOutputStream(out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            while (!Thread.interrupted()) {
                try {
                    Kernel k = (Kernel) jQueue.take();
                    try {
                        k.setOos(oos);
                        k.run();
                    } catch (RuntimeException e) {
//                        TODO: handle users error by special 'not recovareble' kernel
                        try {
                            oos.flush();
                            oos.close();
                        } catch (ClosedByInterruptException e1){
                            System.out.println("Output stream was closed");
                        }
//                        killAll(Thread.currentThread().getId());
                    }
                } catch (InterruptedException e) {
                    System.out.println("act thread " + Thread.currentThread().getName() + " was interrupted in act run");
//                    this.interrupt();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("act thread " + Thread.currentThread().getName() + " was terminated");
        }
    }
    private class PoolReactWorker extends Thread {
        private BlockingQueue<Kernel> rQueue;

        public PoolReactWorker(){
            rQueue = new LinkedBlockingQueue<>();
        }

        private Path logPath;
        private ObjectOutputStream oos;

        public void send (Kernel ker) throws InterruptedException {
            rQueue.put(ker);
        }

        public void run () {
            logPath = Paths.get(_dir+"/"+Thread.currentThread().getId() + "-react-log.log");
            if (Files.exists(logPath)) {
                ObjectInputStream ios;
                boolean isCreated = false;

                try (InputStream in = new BufferedInputStream(Files.newInputStream(logPath))) {
                    ios = new ObjectInputStream(in);
                    Kernel k = (Kernel) ios.readObject();
                } catch (IOException e) {
                    if (e.getClass() == StreamCorruptedException.class || e.getClass() == EOFException.class){
                        try {
                            OutputStream out = new BufferedOutputStream(
                                    Files.newOutputStream(logPath));
                            oos = new ObjectOutputStream(out);
                            isCreated = true;
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        } finally {
                            System.out.format("initialize oos with header \n");
                        }
                    } else {
                        System.out.format("Another type of IO exception occured %s", e.getClass());
                        return;
                    }
                } catch (ClassNotFoundException e) {
                    System.out.format("CNF exception while reading %s", logPath);
                    return;
                }
                if (!isCreated){
                    try {
                        OutputStream out = new BufferedOutputStream(
                                Files.newOutputStream(logPath, CREATE, APPEND));
                        oos = new AppendingObjectOutputStream(out);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    Files.createFile(logPath);
                    OutputStream out = new BufferedOutputStream(
                            Files.newOutputStream(logPath));
                    oos = new ObjectOutputStream(out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            while (!Thread.interrupted()) {
                try {
                    Kernel k = (Kernel) rQueue.take();
                    try {
                        k.setOos(oos);
                        k.run();
                    } catch (RuntimeException e) {
                        try {
                            oos.flush();
                            oos.close();
                        } catch (ClosedByInterruptException e1){
                            System.out.println("Output stream was closed");
                        }
//                        killAll(Thread.currentThread().getId());
//                        Thread.currentThread().interrupt();
                    }
                } catch (InterruptedException e) {
                    System.out.println("react thread "+ Thread.currentThread().getName() +" was interrupted in react run");
//                    this.interrupt();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("react thread "+ Thread.currentThread().getName() +" was terminated");

        }
    }
}
