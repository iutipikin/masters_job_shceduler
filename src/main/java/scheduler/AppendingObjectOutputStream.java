package scheduler;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * Created by yurii on 23.04.2016.
 */
public class AppendingObjectOutputStream extends ObjectOutputStream {
    public AppendingObjectOutputStream(OutputStream out) throws IOException {
        super(out);
    }
    @Override
    protected void writeStreamHeader() {
        // do not write a header, but reset:
        // this line added after another question
        // showed a problem with the original
        try {
            reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
