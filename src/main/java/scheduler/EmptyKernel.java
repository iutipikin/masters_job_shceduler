package scheduler;

/**
 * Created by iutipikin on 4/16/16.
 */
public class EmptyKernel extends Kernel {

    public EmptyKernel (Integer id){
        set_id(id);
    }
    public Status act (){ return new Status(false);}
    public void react (Kernel ker){}
    public void rollback (){}
}
