import scheduler.EmptyKernel;
import scheduler.JobScheduler;
import scheduler.Kernel;
import scheduler.Status;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by iutipikin on 4/15/16.
 */
public class myKernel2 extends Kernel implements Serializable{
    private static final long serialVersionUID = 3L;
    private List<String> childs;
    private boolean resTest;

    public myKernel2 (JobScheduler _js, Kernel pk){
        super(_js, pk);
        childs = new ArrayList<>();
        resTest = true;
    }

    public Status act () {
        try {
            Thread.sleep(1000);
            if (resTest && (Math.random() * 10 > 9))
                throw new RuntimeException();
            for (int i =0; i< 3; i++){
//                myKernel mk = new myKernel(js, this);
//                childs.add(mk.get_id().toString());
//                js.send(mk);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return new Status(false);
        } catch (RuntimeException e){
            System.out.println("RE in MK2: " + get_id() + "\n ");
            System.exit(1);
//            e.printStackTrace();
//            throw new RuntimeException();
        }
        return new Status(true);
    }
    public void react(Kernel ker) {
        childs.remove(ker.get_id().toString());
        if (childs.size() == 0){
            this.reactCondition = true;
            js.sendReact(this);
        }
    }
    public void rollback () {
        System.out.println("rollback mk2 from " + this.get_id());
        resTest = false;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        try {
            oos.writeObject(childs);
            oos.writeBoolean(resTest);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    private void readObject(ObjectInputStream ios) throws IOException, ClassNotFoundException {
        try{
            childs = (List<String>) ios.readObject();
            resTest = ios.readBoolean();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
