import scheduler.JobScheduler;
import scheduler.Kernel;
import scheduler.Status;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Yuri on 05.03.2016.
 */
public class myKernel extends Kernel implements Serializable {
    private static final long serialVersionUID = 4L;
    private Double key;
    protected Double res;

    public myKernel (JobScheduler _js, Kernel pk, Double _key){
        super(_js, pk);
        reactCondition = true;
        key = _key;
        res = 0.0;
    }
    public Status act () {
        try {
            for (int i=0; i < 5000000/key; i++){
                res += (Math.tan(i+10)*Math.sin(i-10))+ 1.34*Math.sqrt(Math.pow(key,2));
            }
        } catch (RuntimeException e) {
            System.out.println("mk interrupted");
            return new Status(false);
        }
        return new Status(false);
    }
    public void react(Kernel ker) {
        System.out.println("react mk: " + ker.get_id() + "-" + getRev());
    }
    public void rollback () {
        System.out.println("rollback myKernel");
    }
    private void writeObject(ObjectOutputStream oos) throws IOException {
        try {
            oos.writeDouble(key);
            oos.writeDouble(res);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    private void readObject(ObjectInputStream ios) throws IOException, ClassNotFoundException {
        try{
            key =  ios.readDouble();
            res =  ios.readDouble();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}