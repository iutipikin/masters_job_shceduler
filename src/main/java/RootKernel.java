import scheduler.EmptyKernel;
import scheduler.JobScheduler;
import scheduler.Kernel;
import scheduler.Status;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yuri on 05.03.2016.
 */

public class RootKernel extends Kernel implements Serializable{
    private static final long serialVersionUID = 2L;
    private Integer reactCounter;
    private List<String> childs;
    private LocalDateTime t1;
    protected Double result;
    protected Long execution;
    protected Integer failedNum;

    public RootKernel (JobScheduler _js, Kernel pk, Integer _failedNum){
        super(_js, pk);
        reactCounter = 0;
        childs = new ArrayList<>();
        result = 0.0;
        execution = 0L;
        failedNum = _failedNum;
    }

    public Status act () {
        System.out.println("act kernel");
        t1 = LocalDateTime.now();
        int i = 0;
        childs = new ArrayList<>();

        double s_val = 0.0;
        for (int j=0; j< 5000000; j++){
            s_val += (Math.sin(j)*0.345 + Math.cos(j*10));
        }
        System.out.println("s_val: " + s_val);
        while (i < 10){
            i++;
            myKernel mk = new myKernel(js, this, s_val+Math.pow(s_val,i));
            childs.add(mk.get_id().toString());
            js.send(mk);
        }
        return new Status(true);
    }
    public void react(Kernel ker) {
        reactCounter++;
        result += ((myKernel) ker).res;
        if (this.restoreTime == null) {
            execution = t1.until(LocalDateTime.now(), ChronoUnit.MILLIS);
            System.out.println("current execution: " + this._id + " " + execution);
            childs.remove(ker.get_id().toString());
            if (childs.size() == 0 ){
                System.out.format("Execution time is: %d, res: %f \n", t1.until(ker.getTimestamp(), ChronoUnit.MILLIS), result);
                reactCondition = true;
                js.sendReact(this);
            }
            if (reactCounter.equals(failedNum))
                System.exit(0);
        } else {
            execution = restoreTime.until(ker.getTimestamp(), ChronoUnit.MILLIS);
            System.out.println("restored execution: " + this._id + " " + execution);
            childs.remove(ker.get_id().toString());
            if (childs.size() == 0 ){
                System.out.format("Restored execution time is: %d, res: %f \n", restoreTime.until(LocalDateTime.now(), ChronoUnit.MILLIS), result);
                reactCondition = true;
                js.sendReact(this);
            }
        }


    }
    public void rollback () {
        System.out.println("rollback root kernel");
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        try {
            oos.writeObject(childs);
            oos.writeInt(reactCounter);
            oos.writeLong(execution);
            oos.writeDouble(result);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    private void readObject(ObjectInputStream ios) throws IOException, ClassNotFoundException {
        try{
            childs = (List<String>) ios.readObject();
            reactCounter = ios.readInt();
            execution = ios.readLong();
            result = ios.readDouble();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
